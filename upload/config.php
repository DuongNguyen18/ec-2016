<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/ec-2016/upload/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/ec-2016/upload/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/ec-2016/upload/catalog/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/ec-2016/upload/system/');
define('DIR_IMAGE', 'C:/xampp/htdocs/ec-2016/upload/image/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/ec-2016/upload/catalog/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/ec-2016/upload/catalog/view/theme/');
define('DIR_CONFIG', 'C:/xampp/htdocs/ec-2016/upload/system/config/');
define('DIR_CACHE', 'C:/xampp/htdocs/ec-2016/upload/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/ec-2016/upload/system/storage/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/ec-2016/upload/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/xampp/htdocs/ec-2016/upload/system/storage/modification/');
define('DIR_UPLOAD', 'C:/xampp/htdocs/ec-2016/upload/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'test');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
