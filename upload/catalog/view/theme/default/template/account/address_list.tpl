<?php echo $header; ?>
<section id="account-so-dia-chi">
<div class= "body_account"> 
    <div class= "container">
      <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
        <!--account_detail_left-->
        <div class="account_detail_left">
            <div class="user_avatar">
              <div class="img">
                <img src="image/images/account/temp/avatar_163x163.jpg" alt="">
              </div>
              <span class="username"><?php echo $firstname." ".$lastname ?></span>
            </div>
            <ul class="menu_account_detail list-unstyled">
              <li><a href="<?php echo $account ?>">Thông tin cá nhân và mật khẩu</a></li>
                  <li class="active"><a href="<?php echo $address ?>">Sổ địa chỉ</a></li>
                  
            </ul>
           </div>
      </div>
            <!--end account_detail_left-->

            <!--account_detail_right-->
            <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
               <div class="account_detail_right right_user_address">
                    <div class="ttl_acc">
                        Quản lý sổ địa chỉ của bạn
                        <p class="desc">Đây là những địa chỉ mà Book sẽ giao hàng đến cho bạn khi đặt hàng</p>
                    </div>
                    <div class="row_user_address_">
                    <?php if ($addresses) { ?>
                    <?php foreach ($addresses as $result) { ?>
                        <div class="four_col col-md-3 col-xs-12 col-sm-6 col-lg-3">
                        <div class="cols_address selected">
                            <div class="ttl_address">
                                <?php if ($result['address_id']==$addressid) echo "Địa chỉ nhận hàng mặc định"; else echo "Địa chỉ nhận hàng phụ" ?>
                            </div>
                            <div class="cont_box_address">
                               
                                <div class="address">
                                    <?php echo $result['address']; ?>
                                </div>
                                <a href="<?php echo $result['update']; ?>" class="btn btn-info">Sửa</a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="btn btn-danger">Xóa</a>
                                <!-- <a href="#" title="Giao đến địa chỉ này" class="btn_select_adr">Giao đến địa chỉ này</a> -->
                            </div>
                        </div>

                      </div>
                      
                      <?php } ?>
                       <?php } else { ?>
                      <p><?php echo $text_empty; ?></p>
                      <?php } ?>
                      
                     
                      
                       
                    </div>
                    <a href="<?php echo $add; ?>" class="btn btn-primary">Thêm địa chỉ mới</a>
        
                    
               </div>
            <!--account_detail_right-->
          </div>
        </div>
        <script type='text/javascript'>window._sbzq||function(e){e._sbzq=[];var t=e._sbzq;t.push(["_setAccount",63930]);var n=e.location.protocol=="https:"?"https:":"http:";var r=document.createElement("script");r.type="text/javascript";r.async=true;r.src=n+"//static.subiz.com/public/js/loader.js";var i=document.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)}(window);</script>
</section>
<?php echo $footer; ?>