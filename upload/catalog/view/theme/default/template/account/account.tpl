<?php echo $header; ?>
<section id="account-thongtinchitiet">
      <div class="clear"></div>
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <div class="account_detail_left">
              <div class="user_avatar">
                <div class="img">
                  <img src="image/images/account/temp/avatar_163x163.jpg" alt="">
                </div>
                <span class="username"><?php echo $firstname ." ". $lastname ?></span>
              </div>
              <ul class="menu_account_detail list-unstyled">
                <li class="active"><a href="<?php echo $account; ?>">Thông tin cá nhân và mật khẩu</a></li>
                <li><a href="<?php echo $address; ?>">Sổ địa chỉ</a></li>
                
                
              </ul>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            <div class="account_detail_right">
              <div class="ttl_acc">
                Quản lý thông tin cá nhân và mật khẩu của bạn
              </div>
              <div class="box_user_inf_">
                <div class="row_user_inf">
                  <label class="lbl">Email đăng nhập</label>
                  <div class="inf">
                    <?php echo $email ?>  
                  </div>
                </div>
                <div class="row_user_inf">
                  <label class="lbl">Mật khẩu</label>
                  <div class="inf password">
                    ******    <a href="<?php echo $password ?>" title="Đổi mật khẩu" class="change_pass">Đổi mật khẩu </a>
                  </div>
                </div>
                <div class="row_user_inf">
                  <label class="lbl">Họ &amp; Tên</label>
                  <div class="inf">
                    <b> <?php echo $firstname ." ". $lastname ?> </b>
                  </div>
                </div>
                <div class="row_user_inf">
                  <label class="lbl">Điện thoại</label>
                  <div class="inf">
                    <?php echo $telephone ?>                      
                  </div>
                </div>
                
                <ul class="list-unstyled" style="float: right;background-color: #f6435b; color: white;padding: 10px;border-radius: 5px;">
                <li>
                <a href="<?php echo $edit ?>" title="Sửa/cập nhật thông tin" class="btn_edit_user_inf " id="btn_edit_user_inf"><b>Sửa/cập nhật thông tin</b></a></li>
                </ul>
              </div>
            
            </div>
          </div>
        </div>
      </div>
      <script type='text/javascript'>window._sbzq||function(e){e._sbzq=[];var t=e._sbzq;t.push(["_setAccount",63930]);var n=e.location.protocol=="https:"?"https:":"http:";var r=document.createElement("script");r.type="text/javascript";r.async=true;r.src=n+"//static.subiz.com/public/js/loader.js";var i=document.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)}(window);</script>
    </section>
<script type="text/javascript"><!--
// Sort the custom fields
$('.form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
		$('.form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('.form-group').length) {
		$('.form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('.form-group').length) {
		$('.form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('.form-group').length) {
		$('.form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});
//--></script>
<?php echo $footer; ?>
