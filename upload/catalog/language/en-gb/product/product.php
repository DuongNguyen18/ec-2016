<?php
// Text
$_['text_search']              = 'Search';
$_['text_brand']               = 'Brand';
$_['text_manufacturer']        = 'Bìa Mềm';
$_['text_model']               = 'Tác Giả:';
$_['text_reward']              = 'Công Ty Phát Hành:';
$_['text_points']              = 'Price in reward points:';
$_['text_stock']               = 'Tình Trạng:';
$_['text_instock']             = 'Còn hàng';
$_['text_tax']                 = 'Ex Tax:';
$_['text_discount']            = ' or more ';
$_['text_option']              = 'Available Options';
$_['text_minimum']             = 'This product has a minimum quantity of %s';
$_['text_reviews']             = '%s reviews';
$_['text_write']               = 'Viết một nhận xét';
$_['text_login']               = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';
$_['text_no_reviews']          = 'Không có nhận xét cho sản phẩm này';
$_['text_note']                = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_success']             = 'Thank you for your review. It has been submitted to the webmaster for approval.';
$_['text_related']             = 'Related Products';
$_['text_tags']                = 'Tags:';
$_['text_error']               = 'Không tìm thấy tên sách';
$_['text_payment_recurring']   = 'Payment Profile';
$_['text_trial_description']   = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']      = '%s every %d %s(s) until canceled';
$_['text_day']                 = 'day';
$_['text_week']                = 'week';
$_['text_semi_month']          = 'half-month';
$_['text_month']               = 'month';
$_['text_year']                = 'year';

// Entry
$_['entry_qty']                = 'Số lượng';
$_['entry_name']               = 'Email';
$_['entry_review']             = 'Nhận xét của bạn';
$_['entry_rating']             = 'Đánh giá';
$_['entry_good']               = 'Tốt';
$_['entry_bad']                = 'Xấu';

// Tabs
$_['tab_description']          = 'Mô tả sách';
$_['tab_attribute']            = 'Specification';
$_['tab_review']               = 'Reviews (%s)';

// Error
$_['error_name']               = 'Warning: Review Name must be between 3 and 25 characters!';
$_['error_text']               = 'Warning: Review Text must be between 25 and 1000 characters!';
$_['error_rating']             = 'Warning: Please select a review rating!';
