<?php
// Heading
$_['heading_title']                  = 'Checkout';

// Text
$_['text_cart']                      = 'Giỏ hàng';
$_['text_checkout_option']           = 'Step %s: Tùy Chọn';
$_['text_checkout_account']          = 'Step %s: Chi Tiết Tài Khoản &amp; Thanh Toán';
$_['text_checkout_payment_address']  = 'Step %s: Chi Tiết Thanh Toán';
$_['text_checkout_shipping_address'] = 'Step %s: Chi Tiết Giao Hàng';
$_['text_checkout_shipping_method']  = 'Step %s: Phương thức vận chuyển';
$_['text_checkout_payment_method']   = 'Step %s: Phương thức thanh toán';
$_['text_checkout_confirm']          = 'Step %s: Xác nhận đơn hàng';
$_['text_modify']                    = 'Modify &raquo;';
$_['text_new_customer']              = 'Khách Hàng Mới';
$_['text_returning_customer']        = 'Khách Hàng Cũ';
$_['text_checkout']                  = 'Tùy Chọn:';
$_['text_i_am_returning_customer']   = 'Tôi là một khách hàng cũ';
$_['text_register']                  = 'Đăng Kí Tài Khoản';
$_['text_guest']                     = 'Khách Thanh Toán';
$_['text_register_account']          = 'Bằng cách tạo tài khoản, bạn sẽ có thể mua sắm nhanh hơn, cập nhật về trạng thái của đơn đặt hàng và theo dõi các đơn hàng bạn đã thực hiện trước đó.';
$_['text_forgotten']                 = 'Quên Mật Khẩu ?';
$_['text_your_details']              = 'Thông tin cá nhân';
$_['text_your_address']              = 'Địa Chỉ';
$_['text_your_password']             = 'Mật khẩu';
$_['text_agree']                     = 'Tôi đã đọc và đồng ý với <a href="%s" class="agree"><b>%s</b></a>';
$_['text_address_new']               = 'Tôi muốn thêm địa chỉ mới';
$_['text_address_existing']          = 'Tôi muốn sử dụng địa chỉ này.';
$_['text_shipping_method']           = 'Vui lòng chọn phương thức vận chuyển ưa thích để sử dụng cho đơn đặt hàng này.';
$_['text_payment_method']            = 'Vui lòng chọn phương thức thanh toán ưa thích để sử dụng cho đơn đặt hàng này';
$_['text_comments']                  = 'Thêm lưu ý cho đơn hàng';
$_['text_recurring_item']            = 'Recurring Item';
$_['text_payment_recurring']         = 'Payment Profile';
$_['text_trial_description']         = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description']       = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']            = '%s every %d %s(s) until canceled';
$_['text_day']                       = 'Ngày';
$_['text_week']                      = 'Tuần';
$_['text_semi_month']                = '2 tuần';
$_['text_month']                     = '1 Tháng';
$_['text_year']                      = 'Năm';

// Column
$_['column_name']                    = 'Tên Sách';
$_['column_model']                   = 'Tác giả';
$_['column_quantity']                = 'Số lượng';
$_['column_price']                   = 'Đơn giá';
$_['column_total']                   = 'Tổng giá';

// Entry
$_['entry_email_address']            = 'E-Mail Address';
$_['entry_email']                    = 'E-Mail';
$_['entry_password']                 = 'Mật khẩu';
$_['entry_confirm']                  = 'Nhập lại mật khẩu';
$_['entry_firstname']                = 'Họ';
$_['entry_lastname']                 = 'Tên';
$_['entry_telephone']                = 'Số Điện Thoại';
$_['entry_fax']                      = 'Fax';
$_['entry_address']                  = 'Choose Address';
$_['entry_company']                  = 'Công Ty';
$_['entry_customer_group']           = 'Customer Group';
$_['entry_address_1']                = 'Địa chỉ 1';
$_['entry_address_2']                = 'Địa chỉ 2';
$_['entry_postcode']                 = 'Mã Bưu điện';
$_['entry_city']                     = 'Thành phố';
$_['entry_country']                  = 'Quốc gia';
$_['entry_zone']                     = 'Khu vực/ Tiển bang';
$_['entry_newsletter']               = 'Nhận thông báo.';
$_['entry_shipping'] 	             = 'Địa chỉ gửi hàng và địa chỉ thanh toán của tôi giống nhau.';

// Error
$_['error_warning']                  = 'Đã xảy ra sự cố khi cố gắng xử lý đơn đặt hàng của bạn! Nếu sự cố vẫn tiếp diễn, hãy thử chọn phương thức thanh toán khác hoặc bạn có thể liên hệ với chủ cửa hàng bằng cách <a href="%s">Bấm vào đây</a>.';
$_['error_login']                    = 'Địa chỉ e-mail hoặc mật khẩu không đúng.';
$_['error_attempts']                 = 'Tài khoản của bạn đã vượt quá số lần đăng nhập cố định. Vui lòng thử lại trong 1 giờ.';
$_['error_approved']                 = 'Tài khoản của bạn yêu cầu xác thực trước khi bạn có thể đăng nhập.';
$_['error_exists']                   = 'E-mail đã được sử dụng!';
$_['error_firstname']                = 'Họ phải từ 1 đến 32 ký tự!';
$_['error_lastname']                 = 'Tên phải từ 1 đến 32 ký tự!';
$_['error_email']                    = 'E-Mail không hợp lệ!';
$_['error_telephone']                = 'Số điện thoại từ 1 đến 32 ký tự!';
$_['error_password']                 = 'Mật khẩu phải từ đến 20 ký tự !';
$_['error_confirm']                  = 'Xác nhậ mật khẩu không đúng!';
$_['error_address_1']                = 'Địa chỉ 1 phải từ 3 đến 128 ký tự!';
$_['error_city']                     = 'Tên công ty 1 phải từ 3 đến 128 ký tự!';
$_['error_postcode']                 = 'Mã bưu điện phải từ 2 đến 10 ký tự!';
$_['error_country']                  = 'Chọn một Quốc gia!';
$_['error_zone']                     = 'Chọn khu vực!';
$_['error_agree']                    = 'Bạn phải đông ý vơi %s!';
$_['error_address']                  = 'Bạn phải chọn một địa chỉ!';
$_['error_shipping']                 = 'Chọn phương thức vận chuyển!';
$_['error_no_shipping']              = 'Không có phương thức vận chuyển nào khả dụng, vui lòng <a href="%s">liên hệ với chúng tôi</a> để được hỗ trợ!';
$_['error_payment']                  = 'Chọn phương thức thanh toán!';
$_['error_no_payment']               = 'Không có phương thức thanh toán nào khả dụng, vui lòng <a href="%s">liên hệ với chúng tôi</a> để được hỗ trợ!';
$_['error_custom_field']             = '%s Bắt buộc!';
