<?php
// Heading
$_['heading_title']            = 'Shopping Cart';

// Text
$_['text_success']             = 'Thành công: Bạn đã thêm <a href="%s">%s</a> vào <a href="%s">shopping cart</a>!';
$_['text_remove']              = 'Thành công: Bạn đã thay đổi shopping cart!';
$_['text_login']               = 'Lưu ý: Bạn phải <a href="%s">đăng nhập </a> hoặc <a href="%s">tạo tài khoản</a> để xem giá!';
$_['text_items']               = '%s Sản phẩm(s) - %s';
$_['text_points']              = 'Reward Points: %s';
$_['text_next']                = 'Bạn muốn làm gì tiếp theo?';
$_['text_next_choice']         = 'Chọn mã giảm giá,điểm thưởng bạn muốn sử dụng hoặc ước tính chi phí giao hàng của bạn.';
$_['text_empty']               = 'Không có sản phẩm trong giỏ hàng!';
$_['text_day']                 = 'ngày';
$_['text_week']                = 'tuần';
$_['text_semi_month']          = 'nửa tháng';
$_['text_month']               = 'tháng';
$_['text_year']                = 'năm';
$_['text_trial']               = '%s every %s %s for %s payments then ';
$_['text_recurring']           = '%s every %s %s';
$_['text_length']              = ' for %s payments';
$_['text_until_cancelled']     = 'until cancelled';
$_['text_recurring_item']      = 'Recurring Item';
$_['text_payment_recurring']   = 'Payment Profile';
$_['text_trial_description']   = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']      = '%s every %d %s(s) until canceled';

// Column
$_['column_image']             = 'Hình ảnh';
$_['column_name']              = 'Tên Sách';
$_['column_model']             = 'Tác giả';
$_['column_quantity']          = 'Sô lượng';
$_['column_price']             = 'Đơn giá';
$_['column_total']             = 'Tổng Giá';

// Error
$_['error_stock']              = 'Products marked with *** are not available in the desired quantity or not in stock!';
$_['error_minimum']            = 'Minimum order amount for %s is %s!';
$_['error_required']           = '%s required!';
$_['error_product']            = 'Không có sản phẩm nào trong giỏ hàng!';
$_['error_recurring_required'] = 'Chọn khoản thanh toán định kỳ!';
