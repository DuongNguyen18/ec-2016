<?php
// Heading
$_['heading_title']        = 'Đăng ký';

// Text
$_['text_account']         = 'Account';
$_['text_register']        = 'Register';
$_['text_account_already'] = 'Đăng ký để nhận những ưu đại đặc biệt từ cửa hàng chúng tôi.';
$_['text_your_details']    = 'Thông tin cá nhân của bạn';
$_['text_your_address']    = 'Your Address';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Mật khẩu của bạn';
$_['text_agree']           = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Customer Group';
$_['entry_firstname']      = 'Tên';
$_['entry_lastname']       = 'Họ';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Số điện thoại';
$_['entry_fax']            = 'Fax';
$_['entry_company']        = 'Company';
$_['entry_address_1']      = 'Address 1';
$_['entry_address_2']      = 'Address 2';
$_['entry_postcode']       = 'Post Code';
$_['entry_city']           = 'City';
$_['entry_country']        = 'Country';
$_['entry_zone']           = 'Region / State';
$_['entry_newsletter']     = 'Subscribe';
$_['entry_password']       = 'Mật khẩu';
$_['entry_confirm']        = 'Xác nhận lại mật khẩu';

// Error
$_['error_exists']         = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']      = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      = 'Telephone must be between 3 and 32 characters!';
$_['error_address_1']      = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']           = 'City must be between 2 and 128 characters!';
$_['error_postcode']       = 'Postcode must be between 2 and 10 characters!';
$_['error_country']        = 'Please select a country!';
$_['error_zone']           = 'Please select a region / state!';
$_['error_custom_field']   = '%s required!';
$_['error_password']       = 'Password must be between 4 and 20 characters!';
$_['error_confirm']        = 'Password confirmation does not match password!';
$_['error_agree']          = 'Warning: You must agree to the %s!';

