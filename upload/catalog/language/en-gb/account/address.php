<?php
// Heading
$_['heading_title']        = 'Sổ địa chỉ';

// Text
$_['text_account']         = 'Tài khoản';
$_['text_address_book']    = 'Sổ địa chỉ';
$_['text_edit_address']    = 'Chỉnh sửa sổ địa chỉ';
$_['text_add']             = 'Địa chỉ của bạn đã thêm thành công ';
$_['text_edit']            = 'Địa chỉ của bạn đã sửa thành công';
$_['text_delete']          = 'Địa chỉ của bạn đã xóa thành công';
$_['text_empty']           = 'You have no addresses in your account.';

// Entry
$_['entry_firstname']      = 'Tên ';
$_['entry_lastname']       = 'Họ';
$_['entry_company']        = 'Tên Công ty';
$_['entry_address_1']      = 'Địa chỉ 1';
$_['entry_address_2']      = 'Địa chỉ 2';
$_['entry_postcode']       = 'Post Code';
$_['entry_city']           = 'Thành Phố';
$_['entry_country']        = 'Quốc Gia';
$_['entry_zone']           = 'Quận/ Huyện';
$_['entry_default']        = 'Địa chỉ mặc định';

// Error
$_['error_delete']                = 'Warning: You must have at least one address!';
$_['error_default']               = 'Warning: You can not delete your default address!';
$_['error_firstname']             = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']              = 'Last Name must be between 1 and 32 characters!';
$_['error_vat']                   = 'VAT number is invalid!';
$_['error_address_1']             = 'Address must be between 3 and 128 characters!';
$_['error_postcode']              = 'Postcode must be between 2 and 10 characters!';
$_['error_city']                  = 'City must be between 2 and 128 characters!';
$_['error_country']               = 'Please select a country!';
$_['error_zone']                  = 'Please select a region / state!';
$_['error_custom_field']          = '%s required!';