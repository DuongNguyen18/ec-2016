<?php
// Heading
$_['heading_title']      = 'Thông tin tài khoản';

// Text
$_['text_account']       = 'Tài khoản';
$_['text_edit']          = 'Chỉnh sửa thông tin';
$_['text_your_details']  = 'Thông tin cá nhân của quý khách';
$_['text_success']       = 'Thành công: Thông tin của bạn đã được lưu lại';

// Entry
$_['entry_firstname']    = 'Tên';
$_['entry_lastname']     = 'Họ';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Số Điện Thoại';
$_['entry_fax']          = 'Fax';

// Error
$_['error_exists']                = 'Warning: E-Mail address is already registered!';
$_['error_firstname']             = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']              = 'Last Name must be between 1 and 32 characters!';
$_['error_email']                 = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']             = 'Telephone must be between 3 and 32 characters!';
$_['error_custom_field']          = '%s required!';