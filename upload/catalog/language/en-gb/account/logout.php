<?php
// Heading
$_['heading_title'] = 'Đăng xuất tài khoản';

// Text
$_['text_message']  = '<p>Bạn đã đăng xuất tài khoản của bạn. Nó sẽ an toàn khi bạn rời khỏi.</p><p>Thẻ mua sắm của bạn đã được lưu , những món hàng sẽ được hiển thị khi bạn đăng nhập lại tài khoản của bạn.</p>';
$_['text_account']  = 'Tài khoản';
$_['text_logout']   = 'Đăng xuất';