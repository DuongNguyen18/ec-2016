<?php
// Heading
$_['heading_title'] = 'Tài khoản của bạn đã được tạo thành công!';

// Text
$_['text_message']  = '<p>Chúc mừng! Tài khoản của bạn đã được tạo thành công!</p> <p>Ngay bây giờ bạn hãy trải nghiệm mua sắm online với chúng tôi</p> <p>Nếu bạn có bất kỳ câu hỏi nào về hệ thống mua sắm online của chúng tôi, hãy e-mail đến cửa hàng của chúng tôi.</p> <p>Xác nhận sẽ được gửi qua mail mà bạn cung cấp . Nếu bạn không nhận thư xác nhận trong vòng 1 giờ , hãy <a href="%s"> liên hệ với chúng tôi </a>.</p>';
$_['text_approval'] = '<p>Cám ơn bạn đã đăng ký với chúng tôi  %s!</p><p>Bạn sẽ được thông báo qua e-mail để kích hoạt tài khoản của bạn.</p><p>Nếu bạn có bất kỳ câu hỏi nào về hệ thống mua sắm online của chúng tôi, hãy <a href="%s">liên hệ với chúng tôi</a>.</p>';
$_['text_account']  = 'Tài khoản';
$_['text_success']  = 'Thành công';
