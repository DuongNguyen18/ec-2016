<?php
// Heading
$_['heading_title']                = 'Đăng nhập';

// Text
$_['text_account']                 = 'Account';
$_['text_login']                   = 'Login';
$_['text_new_customer']            = 'New Customer';
$_['text_register']                = 'Đăng ký tài khoản';
$_['text_register_account']        = 'Đăng nhập để nhận những ưu đại đặc biệt từ cửa hàng chúng tôi.';
$_['text_returning_customer']      = 'Đăng nhập';
$_['text_i_am_returning_customer'] = 'Đăng nhập bằng';
$_['text_forgotten']               = 'Quên mật khẩu?';

// Entry
$_['entry_email']                  = 'E-Mail Address';
$_['entry_password']               = 'Password';



// Error
$_['error_login']                  = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_attempts']               = 'Warning: Your account has exceeded allowed number of login attempts. Please try again in 1 hour.';
$_['error_approved']               = 'Warning: Your account requires approval before you can login.';